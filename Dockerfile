FROM python:2

ENV WORKERS 10

RUN echo "deb http://ftp.osuosl.org/pub/mariadb/repo/10.0/debian wheezy main" > /etc/apt/sources.list.d/mariadb.list
RUN apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
RUN apt-get update

RUN apt-get install -y curl libsasl2-dev python-dev libssl-dev libldap2-dev freetds-dev libjpeg62-dev unixodbc-dev libmariadbclient18 libmariadbclient-dev rabbitmq-server supervisor

RUN mkdir -p /var/log/supervisor
ADD supervisor.conf /etc/supervisor/conf.d/supervisord.conf

RUN curl https://bootstrap.pypa.io/get-pip.py | python
RUN ln -s /usr/local/bin/pip /usr/bin/pip

RUN mkdir /opt/app
ADD . /opt/app

RUN pip install -r /opt/app/requirements.txt
RUN pip install gunicorn

EXPOSE 8000 80
WORKDIR /opt/app
CMD ["/usr/bin/supervisord"]
