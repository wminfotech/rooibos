import os
import sys

sys.path.append('rooibos/')
sys.path.append('rooibos/contrib')

os.environ['DJANGO_SETTINGS_MODULE'] = 'rooibos.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
